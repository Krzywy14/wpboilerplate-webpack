# Wordpress k14theme Theme2.0.0

## Before start ues template, you need this: 
- [Node.js](https://nodejs.org/en/)
- Globaly instaled [python 2](https://www.python.org/downloads/release/python-272/) 
- Recommend use [yarn](https://yarnpkg.com/en/)

## Work rules:
- changes makee only in _dev_ dir
- use [BEM](https://nafrontendzie.pl/metodyki-css-2-bem/)
- use [SASS/SCSS](http://www.merixstudio.pl/blog/co-daje-sass-dlaczego-warto-uzywac-sass/)
- use [es2015+](https://babeljs.io/learn-es2015/) [PL](http://mateuszroth.pl/komp/article-javascriptes6.php)
- follow eslint jquery [rules](https://eslint.org/docs/rules/)
- use tabs indient


## Geting start
1. Download and install wordpress.
2. By comand line get into wp-content/themes.
```bash
cd wp-content/themes/
```
3. Clone repository.
```bash
git clone git@bitbucket.org:Krzywy14/k14theme.git
```
5. Get into dev folder.
```bash
cd dev
```
6. Install required modules by package manager.
```bash
yarn install
```
OR
```bash
npm install
```
6. Start webpack.
```bash
yarn start
```
OR
```bash
npm start
```

## Project scripts
#### START
```bash
yarn start
```
- mode: develop
- watching file changes
- uncompresed files
- source map

#### BUILD
```bash
yarn build
```
- mode: production
- generate css nad js files
- minify files
- source map

* * *

## Include
- [Normalize.scss](http://nicolasgallagher.com/about-normalize-css/)
- [Bootstrap v4 - beta](https://getbootstrap.com/docs/4.0/)
- [Responsive navigation](https://codepen.io/Krzywy14/pen/WpMwXN)
- Plugins
	- [Tabs swither](#tab-switcher)
	- [Scroll to top](#scroll-to-top)
	- [Scroll stop](#scroll-stop)
	- [Anchor navigation](#anchor-navigation)
	- [Parallax](#parallax)
	- [Cookies](#cookies)
	- [Counters](#counters)
	- [Accordion](#accordion)
	- [Gallery](#gallery)
	- [Curtain](#curtain)
	- [Word sizer](#word-sizer)
	- [Preloader](#preloader)
	- [Image zoom on hover](#image-zoom-on-hover)
- IE 10 if
- Wordpress fucntion:
	- Custom content lenght
	- Image functions:
		- [get_img](#simple) - Return html <img> with src and alt. Based on assets/img dir.
		- [get_bg](#simple-background) - Return html inline styl with background-image. Based on assets/img dir.
		- [get_imgth](#WP-thumbnail) - Return html <img> with src and alt. Based on WP tumbnail img.
 		- [get_bgth](#WP-thumbnail-background) - Return html inline styl with background-image. Based on WP thumbnail.
	- Custom login page - must be active in function.php
	- Remove dashboard menu item - must be active in function.php
	- Remove editor from dashboard

* * *

#Changelog
## 2.0.0
- webpack 5
- File structure chagesa
- eslint
- webpack image compressr
## 1.2.0
- Update Gulp to 4.0
- Rebuild task for gulp 4.0
- Fix Build task
## 1.1.3
- package.json update
- Lightbox Update( add gesture/swipe, add arrow key trigers, add counter, add escape close)
- Add rect(height) mixin, lifehac for easy squeres or rects always responsiwe
- Add RWD lazy load background images php ang js, get_rwdbg($postID=null,$smallSize='medium_large',$hugeSize='large',$classes='')
## 1.1.2
- Rebuild cookies plugin to localstorage
- Update gulp task, Add produciton script and style task
- Remove nodeEnv, css compres from config
- Include default cookies plugin 
- Rebuild WP helpers 'echo' to 'return'
- fixes typo
- 404 translation add
- add default translation template
- fix header logo
- fix copyright
## 1.1.1
- Add Wordpress 5 block styles
- Update and rebuild gallery/lightbox
- Add lightbox swipe
- Move wp core style to components/wp/*
- Trun off default wp-block styles at frontend