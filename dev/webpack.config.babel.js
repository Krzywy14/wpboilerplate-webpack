import webpack 						from 'webpack';
import autoprefixer 				from 'autoprefixer';
import path 						from 'path';
import WebpackNotifierPlugin 		from 'webpack-notifier';
import MiniCssExtractPlugin 		from 'mini-css-extract-plugin';
import OptimizeCSSAssetsPlugin 		from 'optimize-css-assets-webpack-plugin';
import UglifyJsPlugin 				from 'uglifyjs-webpack-plugin';
import BrowserSyncPlugin 			from 'browser-sync-webpack-plugin';
import CopyPlugin 					from 'copy-webpack-plugin';
import ImageMinimizerPlugin 		from 'image-minimizer-webpack-plugin';
const appVersion 					= require('./package.json').version;

// eslint-disable-next-line no-unused-vars
module.exports = (env, argv) => {
	return {
		context: path.resolve(__dirname, './src'),
		entry: {
			main: [ './assets/scripts/scripts.js', './assets/styles/style.scss' ],
			preloader: './assets/styles/preloader.scss',
			wpdashboard: './assets/styles/wpstyles/wpdashboard.scss',
			wplogin: './assets/styles/wpstyles/wplogin.scss',
			customizer: './assets/scripts/customizer.js'
		},
		output: {
			path: path.resolve(__dirname, '../'),
			chunkFilename: `assets/scripts/[name].js?v=${appVersion}`,
			filename: `assets/scripts/[name].js?v=${appVersion}`
		},
		module: {
			rules: [
				{
					enforce: 'pre',
					exclude: /node_modules/,
					test: /\.js$/,
					loader: 'eslint-loader'
				},
				{
					test: /\.(png|jpe?g|gif)$/i,
					loader: 'file-loader',
					options: {
						name: '[path][name].[ext]'
					}
				},
				{
					test: /\.(js)$/,
					exclude: /node_modules/,
					use: [ 'babel-loader' ]
				},
				{
					test: /\.(scss|css)$/,
					exclude: [ /webfonts/ ],
					use: [
						argv.mode === 'production' ? MiniCssExtractPlugin.loader : 'style-loader',
						{ loader: 'css-loader', options: { sourceMap: true } },
						{ loader: 'postcss-loader', options: { sourceMap: true } },
						{ loader: 'sass-loader', options: { sourceMap: true } }
					]
				}
			]
		},
		plugins: [
			new WebpackNotifierPlugin(),
			new MiniCssExtractPlugin({
				filename: `assets/styles/[name].css?v=${appVersion}`,
				chunkFilename: `assets/styles/[name].[hash].module.css?v=${appVersion}`
			}),
			new webpack.LoaderOptionsPlugin({
				options: {
					postcss: [
						autoprefixer()
					]
				}
			}),
			new BrowserSyncPlugin({
				files: './src/**/*.php',
				proxy: 'localhost'
			}),
			new CopyPlugin({
				patterns: [
					{
						from: '**/*.php',
						to: ''
					},
					{
						from: 'languages/*',
						to: ''
					}
				]
			}),
			new ImageMinimizerPlugin({
				minimizerOptions: {
					plugins: [
						'gifsicle',
						[ 'mozjpeg', { quality: 80 } ],
						[ 'optipng', { optimizationLevel: 5 } ],
						[ 'svgo', {
							plugins: [
								{ removeViewBox: false }
							]
						} ]
					]
				}
			})
		],
		resolve: {
			modules: [
				'node_modules'
			],
			extensions: [ '.js', '.jsx' ],
			alias: {
				utils: path.resolve(__dirname, 'src/assets/styles/utils/'),
				img: path.resolve(__dirname, 'src/assets/img/'),
				components: path.resolve(__dirname, 'src/assets/styles/components/')
			}
		},
		devtool: 'source-map',
		externals: {
			'jquery': 'jQuery'
		},
		optimization: {
			minimize: argv.mode === 'production' ? true : false,
			minimizer: [
				new OptimizeCSSAssetsPlugin({}),
				new UglifyJsPlugin({
					sourceMap: true
				})
			]
		}
	};
};
