<header class="parent parent--header">
	<section class="row container header__wrap">
		<input type="checkbox" id="navToggler">
		<a href="<?php echo get_home_url() ?>" class="header__logo">
			<img src="<?php echo get_template_directory_uri()?>/assets/img/logo.png" alt="<?= bloginfo('title') ?>">
		</a>
		<label for="navToggler" id="mobile-hamburger" onclick>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</label>
		<nav class="header__nav">
			<?php wp_nav_menu() ?>
		</nav>
	</section>
</header>