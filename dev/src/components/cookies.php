<div class="cookie-bar__wrap" id="cookieBar">
	<div class="cookie-bar__desc">
		<?= __( 'This site uses cookies to work properly.', 'k14theme' ) ?>
		<a href="<?php get_template_directory_uri();?>/polityka-prywatnosci" class="cookie-bar__more"><?= __( 'More', 'k14theme' ) ?></a>
	</div>
	<div class="cookie-bar__accept" id="cookieBarAccept"><?= __( 'Accept', 'k14theme' ) ?></div>
</div>