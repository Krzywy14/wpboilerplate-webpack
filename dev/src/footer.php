<?php
/**
 *
 * @package k14theme
 */

?>
	<footer class="parent parent--footer">
		<section class="row container footer__wrap">
			Footer
		</section>
		<section class="row container copyright__wrap">
			&copy; <?php bloginfo('title'); ?> <?= date("Y"); ?>
		</section>
	</footer>
</div> 

<?php if( $_SERVER["REMOTE_ADDR"] !== '127.0.0.1' ) : ?>
	<link rel="stylesheet" id="scss-css" href="<?=get_template_directory_uri()?>/assets/styles/main.css" type="text/css" media="all">
<?php endif; ?>

<?php wp_footer(); ?>
</body>
</html>