<?php
/**
 *
 * @package k14theme
 */

?>

<?php get_header(); ?>
	<main class="parent parent--404">
		<div class="row container">
			<div class="col col--12 error404__wrap">
				<h1 class="error404__title">404</h1>
				<h2 class="error404__content"><?= __( 'Unfortunately, the page you are trying to access is not available or does not exist', 'k14theme' ) ?></h2>
				<a href="<?php echo get_home_url() ?>" class="button button--404 button--center">
					<?= __( 'Back to main page', 'k14theme' ) ?>
				</a>
			</div>
		</div>
	</main>
<?php get_footer(); ?>