import $	from 'jquery';

export default (selector="")=>{
	const counter = (current) =>{
		$("#counter").html( `${current+1} z ${$(selector).length}`);
	}
	const nextImg = () =>{
		if( $(selector + ".this").next().length ){
			$(selector + ".this").removeClass('this').next().addClass('this');
			let _2 = $(selector + ".this").find('a').attr("href");
			$("#image_extender #container img").attr("src", _2 );
			counter($(selector).index($(selector + ".this")))
		}
	};
	const prevImg = () =>{
		if( $(selector + ".this").prev().length ){
			$(selector + ".this").removeClass('this').prev().addClass('this');
			let _2 = $( selector +".this").find('a').attr("href");
			$("#image_extender #container img").attr("src", _2 );
			counter($(selector).index($(selector + ".this")))
		}
	};
	const closeLightbox = () =>{
		$("#image_extender").fadeOut();
		$(selector + ".this").removeClass('this');
	};
	const handleGesure = () =>{
		if (touchendX < touchstartX) {
			nextImg();
		}
		if (touchendX > touchstartX) {
			prevImg();
		}
	}
	if(selector.length){
		$(selector).click((e)=>{
			e.preventDefault();
			let _src = $(e.currentTarget).find('a').attr("href");
			$(e.currentTarget).addClass('this');
			$("#image_extender #container img").attr("src", _src);
			$("#image_extender").fadeIn();
			counter($(selector).index(e.currentTarget))
		});
		$("#image_extender #exit").click(()=>{
			closeLightbox()
		});
		$("#image_extender #next-ie").click( (e)=>{
			nextImg();
		});
		$("#image_extender #prev-ie").click( (e)=>{
			prevImg();
		});

		let gesuredZone = $('#image_extender');

		let touchstartX = 0;
		let touchstartY = 0;
		let touchendX = 0;
		let touchendY = 0;

		gesuredZone.on({
			touchstart: (event)=>{
				touchstartX = event.changedTouches[0].screenX;
			},
			mousedown: (event)=>{
				touchstartX = event.offsetX;
			},
			touchend: (event)=>{
				touchendX = event.changedTouches[0].screenX;
				handleGesure();
			},
			mouseup: (event)=>{
				touchendX = event.offsetX;
				handleGesure();
			},
		});
		$("body").on({
			keydown: (event)=>{
				if(event.keyCode == 37){
					prevImg();
				}
				if(event.keyCode == 39){
					nextImg();
				}
				if(event.keyCode == 27){
					closeLightbox();
				}
			},
		});
	}
}