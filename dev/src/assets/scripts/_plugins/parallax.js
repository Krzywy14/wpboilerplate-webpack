import $	from 'jquery';

export default (selector)=>{
	if( $(window).width() >  576){
	const speed = 0.3;
	
		if(selector.length){
			$(window).scroll(()=>{
				if( ($(selector).height() + $(selector).offset().top) >= $(window).scrollTop() ){
					let scrollTop = $(window).scrollTop();
					let pos = scrollTop - $(selector).offset().top;
					$(selector).css("background-position","center calc(40% + " + pos*speed +"px)");
				}
			})
		}
	}
}