import $    from 'jquery';


export default function( selector ){
	$(selector+'__content').hide();
	$(selector).on('click',selector+'__title',function() {
		var t = $(this);
		var p = t.parent().siblings().find(selector+'__content');
		var tp = t.next();
		var temp = 200;
		
		if ( t.hasClass('active')){
				t.removeClass('active');
		} else{
			$(selector+'__title').removeClass('active');
			 t.toggleClass('active');
		};
		p.slideUp(temp);
		tp.slideToggle(temp);
	});
}