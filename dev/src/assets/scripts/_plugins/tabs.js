import $ 	from 'jquery';

export default function( nav_selector, content ) {
	if(content != undefined && nav_selector.length != undefined){
		nav_selector
		$(nav_selector + "> li:first-child").addClass('active');
		$(nav_selector + "> li").click(function(){
			var index = $(nav_selector + " li").index( this );

			$(nav_selector + "> li").removeClass("active");
			$(nav_selector + "> li").eq(index).addClass("active");
			
			$(content + "> li").css("visibility","hidden");
			$(content + "> li").eq(index).css("visibility","visible");
				$(content).height( $(content + "> li").eq(index).outerHeight() );
		})
	}
}