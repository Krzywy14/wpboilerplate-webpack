import $					from 'jquery';
import cookies 				from './_plugins/cookies';

// import lightbox				from './_plugins/lightbox';
// import tabs 				from './_plugins/tabs';
// import scrollTop 		from './_plugins/scrollTop';
// import userScroll 		from './_plugins/scrollStop';
// import anchorNav 			from './_plugins/anchorNav';
// import parallax 			from './_plugins/parallax';
// import wordSizer 		from './_plugins/wordSizer';

$(() => {
	$(window).scroll(() => {
		if ($(window).scrollTop() > 100) {
			$('.parent--header').addClass('sticky');
		} else {
			$('.parent--header').removeClass('sticky');
		}
	});

	/**
	 *
	 * I know it is not SIMPLY but this is only way to do wp tables responsive
	 * That code add title attribute to all wp-block-table td
	 *
	 */
	// eslint-disable-next-line no-unused-vars
	$('.wp-block-table').each((tableIndex, table) => {
		$(table).find('tr').each((rowIndex, rowItem) => {
			if (rowIndex > 0) {
				$(rowItem).find('td').each((tdIndex, tdItem) => {
					$(tdItem).attr('data-tdhead', $($($(table).find('tr')[ 0 ]).find('td')[ tdIndex ]).text());
				});
			}
		});
	});

	// lazy load exemple
	// setTimeout(
	// 	() => import(/* webpackChunkName: 'chunks' */ './_plugins/lightbox').then(lightbox => lightbox.default('.blocks-gallery-item'))
	// 	, 2000);

	const loadBackground = (selector) => {

		// selector must by css class or id and had data-smallbg data-hugebg
		if ($(selector).length > 0) {

			// eslint-disable-next-line no-unused-vars
			$(selector).each((index, item) => {

				if ($(window).width() <= 768 && $(item).attr('data-smallbg')) {
					$(item).css('background-image', `url(${$(item).attr('data-smallbg')})`);

				} else if ($(item).attr('data-hugebg')) {
					$(item).css('background-image', `url(${$(item).attr('data-hugebg')})`);
				}
			});
		}
	};

	$(window).ready(() => {
		loadBackground('.loadBackground');
	});

	$(window).resize(() => {
		loadBackground('.loadBackground');
	});

	// Initialize tabs switcher
	// $(()=>{
	// 	new tabs('.tabs__nav', '.tabs__wrap');
	// });

	// Initialize ScrollTop
	// $(()=>{
	// 	new scrollTop();
	// });

	// Initialize user scroll stop achor, scroll top
	// $(()=>{
	// 	new userScroll();
	// });

	// Initialize anchor navigation
	// $(()=>{
	// 	new anchorNav();
	// });

	// Initialize paralax
	// $(()=>{
	// 	new parallax('#parallax');
	// });

	// Initialize Cokies
	new cookies();
});
