<?php
/**
 *
 * @package k14theme
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<link rel="icon" href="<?= get_template_directory_uri() ?>/assets/img/favicon.png">
	<link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/assets/styles/preloader.css" type="text/css" media="all">
	<script type="text/javascript">
		var Preloader = function() {
			document.getElementById("loader").removeAttribute("class");
			document.getElementById("loader").style.opacity = "0";
		};
		window.onload = Preloader;
	</script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="loader" class="loader"></div>
<!--[if lt IE 10]>
	<div class="alert alert-warning">
		You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
	</div>
<![endif]-->
	<?php 
		get_template_part('components/image-extender'); 
		get_template_part('components/cookies');
	?>
	<!-- <div id="go_to_top"></div> -->
<div class="wrapper">
	<?php  get_template_part('components/header'); ?>