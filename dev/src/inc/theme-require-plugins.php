<?php
add_action( 'admin_notices', 'k14theme_dependencies' );

function k14theme_dependencies() {
	$requiredPlugins = [
		// [
		// 	'name' 				=> 'SVG Support',
		// 	'path' 				=> 'svg-support/svg-support.php',
		// ],
	];
	foreach ($requiredPlugins as $key => $plugin) {
		if (!is_plugin_active($plugin['path'])) {
		  	echo '<div class="error"><p>Warning: The theme needs Plugin ' . $plugin['name'] . ' to function</p></div>';
		}
	}
}