<?php

// Custom post type offer

if (!function_exists('create_offer_posttype')) {
	function create_offer_posttype() {
		$labels = array(
			'name' => __( 'Oferty' ),
			'singular_name' => __( 'Oferta' )
		);
		$args = array(
			'labels' 		=> $labels,
			'public' 		=> true,
			'has_archive' 	=> false,
			'hierarchical'	=> true,
			'menu_icon'		=> 'dashicons-yes',
			'supports'		=> array('title','editor','page-attributes'),
			'rewrite' 		=> array('slug' => 'oferta'),
		);
		register_post_type( 'offer', $args);
	}
	add_action( 'init', 'create_offer_posttype' );
}
?>