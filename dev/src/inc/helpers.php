<?php 
/**
 * k14theme Helpers
 *
 * @package k14theme
 */

/*======================================
=            Image function            =
======================================*/

// Return html <img> with src and alt. Based on assets/img dir.
if(!function_exists('get_img')){
	function get_img($src="",$class=""){
		if(!empty($src)){
			$class = !empty($class) ? $class : " ";
			return '<img class="image '.$class.'" src="'.get_template_directory_uri().'/assets/img/'.$src.'" alt="'.$src.'">';
		}
	}
}
// Return html inline styl with background-image. Based on assets/img dir.
if(!function_exists('get_bgimg')){
	function get_bgimg($src=""){
		if(!empty($src)){
			return 'style="background-image: url('.get_template_directory_uri().'/assets/img/'.$src.')"';
		}
	}
}
// Return html <img> with src and alt. Based on WP tumbnail img.
if(!function_exists('get_imgth')){
	function get_imgth($src="",$class=""){
		if(!empty($src)){
			$class = !empty($class) ? $class : " ";
			return '<img class="image '.$class.'" src="'.$src.'" alt="'.$src.'">';
		}
	}
}
// Return html inline styl with background-image. Based on WP thumbnail.
if(!function_exists('get_bgth')){
	function get_bgth($src=""){
		if(!empty($src)){
			return 'style="background-image: url('.$src.')"';
		}
	}
}
// Return html lazy load bg.
if(!function_exists('get_rwdbg')){
	function get_rwdbg($postID=null,$smallSize='medium_large',$hugeSize='large',$classes=''){
		return '<div class="loadBackground '.$classes.'" 
			data-smallbg="'.get_the_post_thumbnail_url($postID,$smallSize).'"
			data-hugebg="'.get_the_post_thumbnail_url($postID,$hugeSize).'"
		></div>';
	}
}
/*=====  End of Image function  ======*/

/*======================================
=            Custom content            =
======================================*/
// Return any post content lenght. Default 50 words.

if(!function_exists('custom_content')){
	function custom_content($size=0){
		if( $size==0 ){$size = 50;}
		$string =  get_the_content();
		$string_arr = explode(' ',$string);
		if(sizeof($string_arr) > $size){
			$string = implode(" ", array_slice($string_arr,0,$size));
			return $string . '...';
		}else{
			return $string;
		}
	}
}
/*=====  End of Custom content  ======*/

/*=============================================
=            Random text generator            =
=============================================*/
// Generate random string lenght. Start from 50 char and end at your param or 399.

if(!function_exists('get_random_text')){
	function get_random_text($max=0){
		$max_r = !empty($max) ? $max : 399;
		$lorem = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci minima, laboriosam amet. Adipisci hic minima a pariatur nam, vero, repudiandae quaerat, repellendus totam voluptates quisquam aliquam placeat est. Qui rem reprehenderit architecto minima, quidem quam, molestias eos veniam, nulla distinctio, asperiores. Harum voluptatem omnis porro quibusdam. Assumenda facilis necessitatibus, modi!";
		$rand = rand(50, $max_r);
		return substr($lorem, 0, $rand); 
	}
}
/*=====  End of Random text generator  ======*/