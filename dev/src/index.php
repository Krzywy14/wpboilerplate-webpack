<?php
/**
 *
 * @package k14theme
 */

?>
<?php get_header(); ?>
	<main class="parent parent--content">
		<section class="row container wp-block__wrap">	
			<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
			<?php endwhile; endif; ?>
		</section>
	</main>
<?php get_footer(); ?>